﻿namespace EFNorthwindPlay.Models
{
    public partial class TenMostExpensiveProductsResult
    {
        public string TenMostExpensiveProducts { get; set; }
        public decimal? UnitPrice { get; set; }
    }
}