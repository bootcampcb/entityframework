﻿using EFNorthwindPlay.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Update;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EFNorthwindPlay
{
    internal class DataLayer
    {
        public static void DisplayCategoryCount()
        {
            var context = new NorthwindContext();
            Console.WriteLine($"Category Count: {context.Categories.Count()}");
        }
        public static void DipslayTotalInventoryValue()
        {
            decimal totInvValue = 0;
            var context = new NorthwindContext();
            foreach (var p in context.Products)
            {
                totInvValue += (decimal)p.UnitsInStock * (decimal)p.UnitPrice;
            }
            Console.WriteLine($"Total Inventory Value: {totInvValue:C2}");
        }
        public static void AddAShipper()
        {
            var context = new NorthwindContext();
            Console.WriteLine("Enter Company Name");
            string companyName = Console.ReadLine();
            Console.WriteLine("Enter phone number");
            string phone = Console.ReadLine();
            Shipper newShipper = new Shipper();
            newShipper.CompanyName = companyName;
            newShipper.Phone = phone;

            context.Shippers.Add(newShipper);
            context.SaveChanges();
        }
        public static void ChangeShipperName()
        {
            var context = new NorthwindContext();
            Console.WriteLine("Enter New Company Name");
            string companyName = Console.ReadLine();
            Console.WriteLine("Enter Shipper ID of company to change");
            int shipperID = int.Parse(Console.ReadLine());

            Shipper shipper = context.Shippers.Find(shipperID);
            if (shipper != null)
            {
                shipper.CompanyName = companyName;
                context.SaveChanges();
                Console.WriteLine($"Company name changed to {companyName}");
            }
            else
            {
                Console.WriteLine("Company not found!");
            }
        }
        public static void DeleteShipper()
        {
            var context = new NorthwindContext();
            Console.WriteLine("Enter Shipper ID of company to delete");
            int shipperID = int.Parse(Console.ReadLine());
            Shipper shipper = context.Shippers.Find(shipperID);
            if (shipper != null)
            {
                context.Shippers.Remove(shipper);
                Console.WriteLine($"Company removed");
            }
            else
            {
                Console.WriteLine("Company not found!");
            }
        }

        public static void DisplayAllProducts()
        {
            var context = new NorthwindContext();
            foreach (var p in context.Products)
            {
                Console.WriteLine(
                $"[{p.ProductId}] {p.ProductName} - {p.UnitPrice:C}, {p.UnitsInStock} on hand");
            }
        }

        public static void DisplayProductsInCategory()
        {
            var context = new NorthwindContext();
            Console.WriteLine("Enter category name:(Beverages-Condiments-Confections-Dairy Products-Grains/Cereals-Meat/Poultry-Produce-Seafood)");
            string catName = Console.ReadLine();

            var prodAndCategoryQuery = context.Categories.Join(context.Products,
                                                               c => c.CategoryId,
                                                               p => p.CategoryId,
                                                              (c, p) => new
                                                              {
                                                                  Category = c.CategoryName,
                                                                  p.ProductId,
                                                                  p.ProductName,
                                                                  p.UnitPrice,
                                                                  p.UnitsInStock
                                                              })
         .Where(cp => cp.Category == catName)
         .OrderBy(cp => cp.Category);
            foreach (var p in prodAndCategoryQuery)
            {
                Console.WriteLine($"[{p.ProductId}] {p.ProductName} - {p.UnitPrice:C}, {p.UnitsInStock} onhand");
            }
        }
        public static void DisplayProductsForSupplier()
        {
            var context = new NorthwindContext();
            Console.WriteLine("Enter supplier name:");
            string supplierName = Console.ReadLine();

            var query =
                 from s in context.Suppliers
                 join p in context.Products
                 on s.SupplierId equals p.SupplierId
                 join c in context.Categories
                 on p.CategoryId equals c.CategoryId
                 where s.CompanyName == supplierName
                 select new
                 {
                     p.ProductId,
                     p.ProductName,
                     p.UnitPrice,
                     p.UnitsInStock,
                     c.CategoryName
                 };
            //$"Categories.CategoryName FROM Products INNER JOIN Suppliers ON Suppliers.SupplierID = Products.SupplierID INNER JOIN Categories ON Categories.CategoryID = Products.CategoryID WHERE Suppliers.CompanyName = '{supplierName}'";
            foreach (var p in query)
            {
                Console.WriteLine(
                $"[{p.ProductId}] {p.ProductName} - {p.UnitPrice:C}, {p.UnitsInStock} onhand - Category: {p.CategoryName}");
            }
        }


        public static void DisplaySupplierProductCounts()
        {
            var context = new NorthwindContext();
            var query = context.Suppliers
                               .GroupJoin(context.Products,
                        s => s.SupplierId,
                        p => p.SupplierId,
                        (s, p) => new
                        {
                            SupplierName = s.CompanyName,
                            CountOfProducts = p.Count()
                        });
            foreach (var p in query)
            {
                Console.WriteLine($"{p.SupplierName} - {p.CountOfProducts}");
            }


        }
        public static void DisplayCustomersandSuppliersByCity()
        {
            string sql = "SELECT * FROM [Customer and Suppliers by City];";

            Console.WriteLine($"{"City",-20} {"Company Name",-40} {"Contact Name",-27} {"Relationship",-20}");



            //Console.WriteLine($"{city,-20} {companyName,-40} {contactName,-27} {relationship,-20}");

        }
        public static void DisplayTenMostExpensiveProducts()
        {
            var context = new NorthwindContext();
            var mostExpensive =
             context.TenMostExpensiveProductsResults.FromSqlInterpolated(
             $"[Ten Most Expensive Products]").ToList();
            foreach (var p in mostExpensive)
            {
                Console.WriteLine(
                $"{p.TenMostExpensiveProducts} -- {p.UnitPrice:C}");
            }
        }
        public static void DisplaySalesByCategory()
        {
            var context = new NorthwindContext();
            Console.WriteLine("Enter category name:(Beverages-Condiments-Confections-Dairy Products-Grains/Cereals-Meat/Poultry-Produce-Seafood)");
            string catName = Console.ReadLine();
            Console.WriteLine("Enter a year(default 1998):");
            int year = int.Parse(Console.ReadLine());

            var salesByCat =
             context.SalesByCategories.FromSqlInterpolated(
             $"[SalesByCategory] @CategoryName={catName}, @OrdYear={year}").ToList();
            foreach (var s in salesByCat)
            {
                Console.WriteLine($"{s.ProductName} - {s.TotalPurchase:C2}");
            }

        }
    }
}
