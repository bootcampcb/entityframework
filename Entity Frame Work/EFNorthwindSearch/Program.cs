﻿using Microsoft.EntityFrameworkCore;
using EFNorthwindSearch;
using EFNorthwindSearch.Models;

Console.WriteLine("Category categories are:");
var context = new NorthwindContext();
var catQuery = from c in context.Categories
               select c;
foreach (Category c in catQuery)
{
    Console.WriteLine($"{c.CategoryName,-15} - {c.Description}");
}
Console.WriteLine("What type of product would you like to see?");
string catName = Console.ReadLine();
var prodAndCategoryQuery =
 context.Categories.Join(context.Products,
 c => c.CategoryId,
 p => p.CategoryId,
(c, p) => new
{
    Category = c.CategoryName,
    p.ProductId,
    p.ProductName,
    p.UnitPrice
})
 .Where (cp => cp.Category == catName)
 .OrderBy(cp => cp.Category);

Console.WriteLine($"{"ID",-2} {"Product",-40} {"Price",-10 }");
Console.WriteLine($"{"--",-2} {"-------",-40} {"-----",-10}");
foreach (var pc in prodAndCategoryQuery)
{
    Console.WriteLine($"{pc.ProductId,-2} {pc.ProductName,-40} {pc.UnitPrice,-10}");
}