﻿using EFBlockbusters.Models;
using Microsoft.EntityFrameworkCore;
public class BlockbusterMovieEntities : DbContext
{
    public DbSet<Category> Categories { get; set; }
    public DbSet<Movie> Movies { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=Movies;Integrated Security=True;Connect Timeout=30;Encrypt=False;Trust Server Certificate=False;Application Intent=ReadWrite;Multi Subnet Failover=False");
    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        //modelBuilder.Entity<Category>()
        //    .HasMany(Category => Category.Movies)
        //    .WithOne(Movie => Movie.Category)
        //    .HasForeignKey(Movie => Movie.CategoryId);

        modelBuilder.Entity<Category>().HasData(
            new Category
            {
                CategoryId = 1,
                CategoryName = "Romantic Comedy"
            },
            new Category
            {
                CategoryId = 2,
                CategoryName = "Action"
            },
            new Category
            {
                CategoryId = 3,
                CategoryName = "Kids"
            },
            new Category
            {
                CategoryId = 4,
                CategoryName = "Science Fiction"
            },
            new Category
            {
                CategoryId = 5,
                CategoryName = "Family"
            },
            new Category
            {
                CategoryId = 6,
                CategoryName = "Western"
            }
            );
        modelBuilder.Entity<Movie>().HasData(
    new Movie
    {
        MovieId = 1,
        Title = "Crazy Rich Asians",
        Description = "A woman discovers her boyfriend comes from one of the wealthiest families in Singapore.",
        Rating = "7.0",
        CategoryId = 1
    },
    new Movie
    {
        MovieId = 2,
        Title = "Deadpool",
        Description = "A wisecracking mercenary gets experimented on and becomes immortal but ugly.",
        Rating = "8.0",
        CategoryId = 2
    },
    new Movie
    {
        MovieId = 3,
        Title = "Despicable Me",
        Description = "A supervillain plans to steal the moon, but he meets three orphan girls who change his heart.",
        Rating = "7.6",
        CategoryId = 3
    },
    new Movie
    {
        MovieId = 4,
        Title = "The Matrix",
        Description = "A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.",
        Rating = "8.7",
        CategoryId = 4
    },
    new Movie
    {
        MovieId = 5,
        Title = "The Lion King",
        Description = "A young lion prince is cast out of his pride by his cruel uncle, but he learns to take back what is his with the help of some new friends.",
        Rating = "8.5",
        CategoryId = 5
    },
    new Movie
    {
        MovieId = 6,
        Title = "The Good, the Bad and the Ugly",
        Description = "A bounty hunting scam joins two men in an uneasy alliance against a third in a race to find a fortune in gold buried in a remote cemetery.",
        Rating = "8.8",
        CategoryId = 6
    },
    new Movie
    {
        MovieId = 7,
        Title = "10 Things I Hate About You",
        Description = "A pretty, popular teenager can't go out on a date until her ill-tempered older sister does.",
        Rating = "7.3",
        CategoryId = 1
    },
    new Movie
    {
        MovieId = 8,
        Title = "Die Hard",
        Description = "An NYPD officer tries to save his wife and several others taken hostage by German terrorists during a Christmas party at the Nakatomi Plaza in Los Angeles.",
        Rating = "8.2",
        CategoryId = 2
    },
    new Movie
    {
        MovieId = 9,
        Title = "Finding Nemo",
        Description = "After his son is captured in the Great Barrier Reef and taken to Sydney, a timid clownfish sets out on a journey to bring him home.",
        Rating = "8.1",
        CategoryId = 3
    },
    new Movie
    {
        MovieId = 10,
        Title = "Inception",
        Description = "A thief who steals corporate secrets through the use of dream-sharing technology is given the inverse task of planting an idea into the mind of a CEO.",
        Rating = "8.8",
        CategoryId = 4
    }
);

    }
}