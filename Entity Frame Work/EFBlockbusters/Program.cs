﻿using EFBlockbusters;
using EFBlockbusters.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

bool exit = true;
static bool ToBoolFuzzy(string stringVal)
{
string normalizedString = (stringVal?.Trim() ?? "false").ToLowerInvariant();
bool result = (normalizedString.StartsWith("y")
    || normalizedString.StartsWith("t")
    || normalizedString.StartsWith("1"));
return result;
}

do
{
    Console.WriteLine("Category categories are:");
    var context = new BlockbusterMovieEntities();
    var catQuery = from c in context.Categories
                   select c;
    foreach (Category c in catQuery)
    {
        Console.WriteLine($"{c.CategoryName,-15}");
    }
    Console.WriteLine("What type of movie would you like to see?");
    string catName = Console.ReadLine();
    var movieAndCategoryQuery =
     context.Categories.Join(context.Movies,
     c => c.CategoryId,
     m => m.CategoryId,
    (c, m) => new
    {
        Category = c.CategoryName,
        m.MovieId,
        m.Title,
        m.Description,
        m.Rating
    })
     .Where(cm => cm.Category == catName)
     .OrderBy(cm => cm.Category);

    Console.WriteLine($"{"ID",-2} {"Movie Title",-40} {"Rating",-10}");
    Console.WriteLine($"{"--",-2} {"-----------",-40} {"------",-10}");
    foreach (var mc in movieAndCategoryQuery)
    {
        Console.WriteLine($"{mc.MovieId,-2} {mc.Title,-40} {mc.Rating,-10}");
        Console.WriteLine($"Description: {mc.Description}");
    }

    Console.WriteLine("Continue?");
    exit = ToBoolFuzzy(Console.ReadLine());
} while (exit);
